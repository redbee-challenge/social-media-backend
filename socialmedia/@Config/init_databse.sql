DROP SCHEMA IF EXISTS social_media;

CREATE SCHEMA social_media;

USE social_media;

CREATE TABLE user (
  user_name         varchar(15) not null,
  user_pass         varchar(15) not null,
  PRIMARY KEY(user_name)
);

CREATE TABLE user_role (
  user_name         varchar(15) not null,
  role_name         varchar(15) not null,
  PRIMARY KEY(user_name, role_name)
);

CREATE TABLE interest (
  id 	  		  int NOT NULL AUTO_INCREMENT,
  type	          varchar(15) not null,
  keyword         varchar(50) not null,
  fk_user_name			varchar(15) not null,
  PRIMARY KEY(id),
  FOREIGN KEY (fk_user_name) REFERENCES user(user_name)
);

INSERT INTO user (user_name, user_pass) VALUES ('redbee', 'r3db33');
INSERT INTO user (user_name, user_pass) VALUES ('diego', 'd13g0');
INSERT INTO user (user_name, user_pass) VALUES ('tomas', 't0m4s');
INSERT INTO user (user_name, user_pass) VALUES ('gonzalo', 'g0nz4l0');

INSERT INTO user_role (user_name, role_name) VALUES ('redbee', 'user');
INSERT INTO user_role (user_name, role_name) VALUES ('diego', 'user');
INSERT INTO user_role (user_name, role_name) VALUES ('tomas', 'user');
INSERT INTO user_role (user_name, role_name) VALUES ('gonzalo', 'user');

INSERT INTO interest (type, keyword, fk_user_name) VALUES ('#', 'boca', 'redbee');
INSERT INTO interest (type, keyword, fk_user_name) VALUES ('@', 'liomessi', 'redbee');
INSERT INTO interest (type, keyword, fk_user_name) VALUES ('#', 'river', 'redbee');
INSERT INTO interest (type, keyword, fk_user_name) VALUES ('#', 'manuginobilli', 'tomas');
INSERT INTO interest (type, keyword, fk_user_name) VALUES ('#', 'river', 'diego');

