package io.redbee.challenge.socialmedia.rest;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.SignatureException;
import io.redbee.challenge.socialmedia.dto.InterestDto;
import io.redbee.challenge.socialmedia.entity.Interest;
import io.redbee.challenge.socialmedia.logic.InterestLogic;
import io.redbee.challenge.socialmedia.security.Security;
import io.redbee.challenge.socialmedia.twitter.TwitterMediator;
import twitter4j.Status;
import twitter4j.TwitterException;
import twitter4j.TwitterObjectFactory;

@Component
@Path("/tweetsAPI")
public class TweetsAPI {

	@Autowired(required=true)
	@Qualifier("twitterApi")
	private TwitterMediator twitterApi;
	
	@Autowired(required=true)
	@Qualifier("interestLogic")
	private InterestLogic interestLogic;


    @POST
    @Path("/fetchTweetsForInterest")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public Response fetchTweetsForInterest(@Context HttpServletRequest request, Interest interestParam) {
    	String username = "";
    	List<String> jsonTweets = null;
   		try {
    		username = Security.parseRequest(request);    	
    		InterestDto interestDto = this.interestLogic.getInterest(interestParam);
    		jsonTweets = twitterApi.fetchInitialTweets(username, interestDto);
		} catch(SignatureException se) {
    		return Response.status(Response.Status.UNAUTHORIZED).build();
    	} catch (TwitterException e) {
			return Response.serverError().entity("Hubo un problema de comunicacion con Twitter.").build();
		}
   		System.out.println(jsonTweets);
		return Security.generateResponse(username, jsonTweets);
	}
    
    @POST
    @Path("/fetchMoreTweetsForInterest")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public Response fetchMoreTweetsForInterest(@Context HttpServletRequest request, Interest interestParam) {
    	String username = "";
    	List<String> jsonTweets = null;
    	try {
    		username = Security.parseRequest(request);    	
	    	InterestDto interestDto = this.interestLogic.getInterest(interestParam);
    		jsonTweets = twitterApi.fetchMoreTweets(username, interestDto);
		} catch(SignatureException se) {
    		return Response.status(Response.Status.UNAUTHORIZED).build();
    	} catch (TwitterException e) {
			return Response.serverError().entity("Hubo un problema de comunicacion con Twitter.").build();
		}
    	return Security.generateResponse(username, jsonTweets);
	}
	
//    @POST
//    @Path("/fetchTweetsForInterest")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//	public Response fetchTweetsForInterest(@Context HttpServletRequest request, Interest interestParam) {
//    	return fetchTweets(request, fetchInitial, interestParam);
//	}
//
//	@POST
//    @Path("/fetchMoreTweetsForInterest")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//	public Response fetchMoreTweetsForInterest(@Context HttpServletRequest request, Interest interestParam) {
//		return fetchTweets(request, fetchMore, interestParam);
//	}
//	
//	private BiFunction<String, Interest, List> fetchInitial = (username, interest) -> {
//		List<Status> list = null;
//		try {
//			list = twitterApi.fetchInitialTweets(username, interest);
//		} catch (TwitterException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return list;
//	};
//	
//	private BiFunction<String, Interest, List> fetchMore = (username, interest) -> {
//		List<Status> list = null;
//		try {
//			list = twitterApi.fetchMoreTweets(username, interest);
//		} catch (TwitterException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return list;
//	};
//	
//	private Response fetchTweets(HttpServletRequest request, BiFunction<String, Interest, List> function, Interest interestParam) {
//		String username = "";
//		try {
//			username = Security.parseRequest(request);
//		}catch(SignatureException se) {
//			return Response.status(401).build();
//		}
//		
//    	Interest interest = this.interestLogic.getInterests(interestParam);
//    	List<Status> tweets = null;
//		tweets = function.apply(username, interest);
//		List<String> jsonTweets = new ArrayList<>(); 
//		
//		tweets.stream()
//		.forEach((tweet) -> {jsonTweets.add(TwitterObjectFactory.getRawJSON(tweet));});
//		
//		System.out.println("TWEEEEETSSS: \n" + jsonTweets);
//		return Response.status(200).entity(tweets).build();
//	}
}
