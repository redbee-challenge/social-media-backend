package io.redbee.challenge.socialmedia.dao;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

@Repository
public class GenericDaoImpl<T, I extends Serializable> extends GenericDao<T, I> implements IGenericDao<T, I> {

}
