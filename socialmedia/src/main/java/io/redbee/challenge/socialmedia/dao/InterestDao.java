package io.redbee.challenge.socialmedia.dao;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import io.redbee.challenge.socialmedia.entity.Interest;

@Repository("interestDao")
@Transactional(propagation=Propagation.REQUIRED)
public class InterestDao extends GenericDao<Interest, Long>{

	@Transactional(readOnly=true)
	public List<Interest> getAllInterestsByUsername(String username) {
		Session session = entityManager.unwrap(Session.class);
		Query query = session.createQuery("from Interest where user.username = :username ");
		query.setParameter("username", username);		
		return query.getResultList();
	}
	
}
