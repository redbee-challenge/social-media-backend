package io.redbee.challenge.socialmedia.security;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Response;

import java.security.Key;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.impl.crypto.MacProvider;

public class Security {
	private static final Key key = MacProvider.generateKey();
	public static final String AUTHORIZATION_HEADER = "Authorization";
	
	public static String parseRequest(HttpServletRequest request) throws SignatureException {
		String compactJws = request.getHeader(AUTHORIZATION_HEADER);		
		return Jwts.parser().setSigningKey(key).parseClaimsJws(compactJws).getBody().getSubject();
	}
	
	public static Response generateResponse(String subject, Object entity) {
		return Response.status(Response.Status.ACCEPTED)
    			.entity(entity)
	        	.header(Security.AUTHORIZATION_HEADER, generateJWSToken(subject))
	        	.build();
	}
	
	private static String generateJWSToken(String subject) {
		String compactJws = Jwts.builder()
				  .setSubject(subject)
				  .signWith(SignatureAlgorithm.HS512, key)
				  .compact();
		return compactJws;
	}
	
}
