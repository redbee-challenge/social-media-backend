package io.redbee.challenge.socialmedia.logic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import io.redbee.challenge.socialmedia.dao.IGenericDao;
import io.redbee.challenge.socialmedia.entity.User;

@Service("userLogic")
@Transactional(propagation=Propagation.REQUIRED)
public class UserLogic {
	private IGenericDao<User, String> userDao;
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class, isolation = Isolation.SERIALIZABLE)
	public User getUserByUsername(String username) {
		User user = userDao.findOne(username);
		user.getInterests();
		return user;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class, isolation = Isolation.SERIALIZABLE)
	public boolean authorizeUser(String username, String password) {
		User user = this.getUserByUsername(username);
		return user.getPassword().equals(password);
	}

	
	@Autowired
	public void setUserDao(IGenericDao<User, String> userDao) {
		this.userDao = userDao;
		this.userDao.setClazz(User.class);
	}
}
