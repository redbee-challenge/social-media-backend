package io.redbee.challenge.socialmedia.rest;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.SignatureException;
import io.redbee.challenge.socialmedia.dto.InterestDto;
import io.redbee.challenge.socialmedia.entity.Interest;
import io.redbee.challenge.socialmedia.entity.User;
import io.redbee.challenge.socialmedia.logic.InterestLogic;
import io.redbee.challenge.socialmedia.logic.UserLogic;
import io.redbee.challenge.socialmedia.security.Security;

@Component
@Path("/interestAPI")
public class InterestAPI {
	
	@Autowired(required=true)
	@Qualifier("interestLogic")
	private InterestLogic interestLogic;
    
    @GET
    @Path("/fetchAllInterests")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllInterests(@Context HttpServletRequest request) {
    	String username;
    	List<InterestDto> interestDtos;
    	try {
    		username = Security.parseRequest(request);
    		interestDtos = this.interestLogic.getAllInterestsByUsername(username);
    	}catch(SignatureException se) {
    		return Response.status(Response.Status.UNAUTHORIZED).build();
    	}
    	System.out.println(interestDtos);
    	return Security.generateResponse(username, interestDtos.toArray());
    } 

    
    @POST
    @Path("/newInterest")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response newInterest(@Context HttpServletRequest request, Interest interestParam) {
    	String username;
    	try {
    		username = Security.parseRequest(request);
    		this.interestLogic.newInterest(username, interestParam);
    	}catch(SignatureException se) {
    		return Response.status(Response.Status.UNAUTHORIZED).build();
    	}
    	return Security.generateResponse(username, null);
    } 

    
    @POST
    @Path("/getInterest")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getInterest(@Context HttpServletRequest request, Interest interestParam) {
    	String username;
    	InterestDto interestDto;
    	try {
    		username = Security.parseRequest(request);
    		interestDto = this.interestLogic.getInterest(interestParam);
    	}catch(SignatureException se) {
    		return Response.status(Response.Status.UNAUTHORIZED).build();
    	}
    	return Security.generateResponse(username, interestDto);
    } 
    
    @PUT
    @Path("/updateInterest")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateInterest(@Context HttpServletRequest request, Interest interestParam) {
    	String username;
    	InterestDto interestDto;
    	try {
    		username = Security.parseRequest(request);
    		interestDto = this.interestLogic.updateInterest(interestParam);
    	}catch(SignatureException se) {
    		return Response.status(Response.Status.UNAUTHORIZED).build();
    	}
    	return Security.generateResponse(username, interestDto);
    } 
    
    @DELETE
    @Path("/deleteInterest")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteInterest(@Context HttpServletRequest request, Interest interestParam) {
    	String username;
    	try {
    		username = Security.parseRequest(request);
    		this.interestLogic.deleteInterest(interestParam);
    	}catch(SignatureException se) {
    		return Response.status(Response.Status.UNAUTHORIZED).build();
    	}
    	return Security.generateResponse(username, null);
    } 
}
