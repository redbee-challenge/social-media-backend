package io.redbee.challenge.socialmedia.dao;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

//@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Repository
public abstract class GenericDao<T, I extends Serializable> {
	@PersistenceContext
	protected EntityManager entityManager;

	private Class<T> clazz;

	public GenericDao() {

	}

	public void setClazz(Class<T> clazzToSet) {
		this.clazz = clazzToSet;
	}

	public T findOne(I id) {
		return entityManager.find(clazz, id);
	}
	
	public List<T> findAll() {
		return entityManager.createQuery("from " + clazz.getName()).getResultList();
	}

	public void save(T entity) {
		entityManager.persist(entity);
	}

	public T update(T entity) {
		return entityManager.merge(entity);
	}

	public void delete(T entity) {
		entityManager.remove(entity);
	}

	public void deleteById(I entityId) {
		T entity = findOne(entityId);
		delete(entity);
	}

}
