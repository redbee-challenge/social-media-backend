package io.redbee.challenge.socialmedia.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import io.redbee.challenge.socialmedia.entity.User;
import io.redbee.challenge.socialmedia.logic.UserLogic;
import io.redbee.challenge.socialmedia.security.Security;

@Component
@Path("/loginAPI")
public class LoginAPI {

	@Autowired(required=true)
	@Qualifier("userLogic")
	private UserLogic userLogic;
	
    @GET
    @Path("/ping")
    @Produces(MediaType.TEXT_PLAIN)
    public String ping() {
    	return "PING!!";
    }  
       
    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(User userParam) {
    	boolean authorized = this.userLogic.authorizeUser(userParam.getUsername(), userParam.getPassword());
    	if(authorized) {
	        return Security.generateResponse(userParam.getUsername(), null);
    	}else {
    		return Response.status(Response.Status.UNAUTHORIZED).build();
    	}
    } 
}
