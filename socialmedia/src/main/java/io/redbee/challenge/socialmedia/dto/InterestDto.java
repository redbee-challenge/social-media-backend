package io.redbee.challenge.socialmedia.dto;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import io.redbee.challenge.socialmedia.entity.User;

public class InterestDto {
	private long id;
	private String type;
	private String keyword;
	
	public InterestDto(long id, String type, String keyword) {
		this.id = id;
		this.type = type;
		this.keyword = keyword;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	
	@Override
	public String toString() {
		return this.type + this.keyword;
	}
}
