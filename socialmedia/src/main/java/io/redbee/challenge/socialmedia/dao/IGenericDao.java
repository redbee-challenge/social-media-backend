package io.redbee.challenge.socialmedia.dao;

import java.io.Serializable;
import java.util.List;

public interface IGenericDao<T, I extends Serializable> {
	T findOne(final I id);

	List<T> findAll();

	void save(final T entity);

	T update(final T entity);

	void delete(final T entity);

	void deleteById(final I entityId);

	void setClazz(Class<T> clazz);
}
