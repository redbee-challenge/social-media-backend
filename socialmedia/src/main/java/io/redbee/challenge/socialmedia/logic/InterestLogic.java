package io.redbee.challenge.socialmedia.logic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import io.redbee.challenge.socialmedia.assembler.InterestAssembler;
import io.redbee.challenge.socialmedia.dao.InterestDao;
import io.redbee.challenge.socialmedia.dto.InterestDto;
import io.redbee.challenge.socialmedia.entity.Interest;
import io.redbee.challenge.socialmedia.entity.User;

@Service("interestLogic")
@Transactional(propagation=Propagation.REQUIRED)
public class InterestLogic {
	
	private InterestDao interestDao;
	
	@Autowired(required=true)
	@Qualifier("userLogic")
	private UserLogic userLogic;
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class, isolation = Isolation.SERIALIZABLE)
	public void newInterest(String username, Interest interest){
		User user = userLogic.getUserByUsername(username);
		interest.setUser(user);
		user.getInterests().add(interest);
		this.interestDao.save(interest);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class, isolation = Isolation.SERIALIZABLE)
	public InterestDto getInterest(Interest interest){
		return InterestAssembler.toDto(this.interestDao.findOne(interest.getId()));
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class, isolation = Isolation.SERIALIZABLE)
	public InterestDto updateInterest(Interest interest){
		Interest interestToUpdate = this.interestDao.findOne(interest.getId());
		interestToUpdate.setType(interest.getType());
		interestToUpdate.setKeyword(interest.getKeyword());
		return InterestAssembler.toDto(this.interestDao.update(interestToUpdate));
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class, isolation = Isolation.SERIALIZABLE)
	public void deleteInterest(Interest interest){
		Interest interestToDelete = this.interestDao.findOne(interest.getId());
		this.interestDao.delete(interestToDelete);
	}
	
	@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class, isolation = Isolation.SERIALIZABLE)
	public List<InterestDto> getAllInterestsByUsername(String username){
		return InterestAssembler.toDtoList(this.interestDao.getAllInterestsByUsername(username));
	}
	
	@Autowired
	@Qualifier("interestDao")
	public void setInterestDao(InterestDao interestDao) {
		this.interestDao = interestDao;
		this.interestDao.setClazz(Interest.class);
	}
}
