package io.redbee.challenge.socialmedia.twitter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import io.redbee.challenge.socialmedia.dto.InterestDto;
import io.redbee.challenge.socialmedia.entity.Interest;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterObjectFactory;

@Component("twitterApi")
public class TwitterMediator {
	private static volatile TwitterMediator twitterApi;
	private Map<String, Map<String, Query>> queryCache;
	private Twitter twitter;
	
	public TwitterMediator() {
		this.twitter = new TwitterFactory().getInstance();
		this.queryCache = new HashMap<String, Map<String, Query>>();
	}
	
	public static TwitterMediator getInstance() {
		if(twitterApi == null) {
			synchronized (twitterApi) {
				if(twitterApi == null) {
					twitterApi = new TwitterMediator();
				}
			}
		}
		return twitterApi;
	}
	
	public List<String> fetchInitialTweets(String username, InterestDto interestDto) throws TwitterException{
		Query query = new Query(interestDto.getType() + interestDto.getKeyword());
		return this.fetchTweets(username, interestDto, query);
	}

	public List<String> fetchMoreTweets(String username, InterestDto interestDto) throws TwitterException{
		Query query = this.getCachedQuery(username, interestDto);
		return this.fetchTweets(username, interestDto, query == null ? new Query(interestDto.toString()) : query);
	}

	private List<String> fetchTweets(String username, InterestDto interestDto, Query query) throws TwitterException{
		QueryResult result = null;
		List<Status> tweets = null;

		result = twitter.search(query);
        tweets = result.getTweets();
        List<String> jsonTweets = new ArrayList<>();
		tweets.stream().forEach((tweet) -> {jsonTweets.add(TwitterObjectFactory.getRawJSON(tweet));});
        
        query = result.nextQuery();
        this.updateQueryCache(username, interestDto, query);
        System.out.println("Json for "+ interestDto.getType() + interestDto.getKeyword() + ": " + jsonTweets);
		return jsonTweets;		
	}
	
	public void fetchTweetsForAllInterests(List<Interest> interests) {
		
	}
	
	/* ************************************** */
	/* ********** CACHE MANAGEMENT ********** */
	/* ************************************** */
	
	private Query getCachedQuery(String username, InterestDto interestDto) {
		// Check if user is cached
		Map<String, Query> queries = queryCache.get(username);
		if(queries == null || queries.isEmpty()) {
			return null;
		}
		
		return queries.get(interestDto.toString());
	}
	
	private void updateQueryCache(String username, InterestDto interestDto, Query query) {
		Map <String, Query> map = this.queryCache.get(username);
		if(map == null) {
			Map<String, Query> newQueryMap = new HashMap<String, Query>();
			newQueryMap.put(interestDto.toString(), query);
			this.queryCache.put(username, newQueryMap);
		}else {
			map.put(interestDto.toString(), query);
		}
	}
	
	private void deleteFromQueryCache(String username, InterestDto interestDto) {
		Map <String, Query> map = this.queryCache.get(username);
		if(map == null) {
			return;
		}
		map.remove(interestDto.toString());
		// if queryResultsCache for the user is empty, then remove user from cache
		if(map.size() < 1) {
			this.queryCache.remove(username);
		}
	}
}
