package io.redbee.challenge.socialmedia.assembler;

import java.util.ArrayList;
import java.util.List;

import io.redbee.challenge.socialmedia.dto.InterestDto;
import io.redbee.challenge.socialmedia.entity.Interest;

public class InterestAssembler {

	public static InterestDto toDto(Interest interest) {
		return new InterestDto(interest.getId(), interest.getType(), interest.getKeyword());
	}
	
	public static Interest fromDto(InterestDto dto) {
		Interest interest = new Interest();
		interest.setId(dto.getId());
		interest.setType(dto.getType());
		interest.setKeyword(dto.getKeyword());
		return interest;
	}
	
	public static List<InterestDto> toDtoList(List<Interest> interestList){
		List<InterestDto> dtoList = new ArrayList<>();
		for(int i = 0; i < interestList.size(); i++) {
			dtoList.add(toDto(interestList.get(i)));
		}
		return dtoList;
	}
}
