package io.redbee.challenge.socialmedia.dao;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import io.redbee.challenge.socialmedia.entity.User;

@Repository("userDao")
@Transactional(propagation=Propagation.REQUIRED)
public class UserDao extends GenericDao<User, String>{


	@Transactional(readOnly=true)
	public User getUserByUsername(String username) {
		return entityManager.find(User.class, username);
	}
}
